<span style="color:PaleGreen; font-size:22pt"> Group Report </span>


[//]: <> (introduction)
<span style="color:coral; font-size:19pt"> Introduction </span><br>

The purpose of this group report is to deliver a detail plan of what we as a team are going to create for our group assignment. We will create a detalied wireframes on [draw.io] to represent how our asp.net core website will look like. We have also mutually decided on team roles. 
> Task Completed by Guri





[//]: <> (team roles allocation)
<span style="color:coral; font-size:19pt"> Team Roles Allocation </span><br>
* Collected resources - Michael Tindall
* Wireframes - Jason Wallace and Gurjinder Ghuman 
* Tools - Liam mason Webb



> Partially decided by team 





[//]: <> (collected resources)
<span style="color:coral; font-size:19pt"> Collected Resources </span>
### We will use [video documentation] on Youtube about the MVC framework in ASPNet
[video documentation]: https://www.youtube.com/playlist?list=PLs5n5nYB22fIqNHp8kHP6dLW5DZYJnWxz

### We will use the Google Maps API from the [Google developer site]
[Google developer site]: https://developers.google.com/maps/

### We will also use [ASPSnippets] and [StackOverflow] for additional documentation on the Google Maps API
[ASPSnippets]: https://www.aspsnippets.com/Articles/Using-Google-Maps-API-in-ASP.Net.aspx
[StackOverflow]: https://stackoverflow.com/questions/37032074/using-google-maps-c-asp-net-sql-server-to-show-map-with-markers-map-not-dis



[//]: <> (collected resources)
<span style="color:red; font-size:17pt"> We may or may not use twitter API (Depending on time available). </span>
### We will use the Twitter API from the [Twitter developer site]. We will also [reuse StackOverflow] for additional documentation on the Twitter API.

[reuse StackOverflow]: https://stackoverflow.com/questions/19828195/twitter-api-integration-in-asp-net
[Twitter developer site]: https://dev.twitter.com/rest/public

> Task completed by Michael Tindall and Liam




[//]: <> (wireframes)
<span style="color:coral; font-size:19pt"> Wireframes </span>

[//]: <> (link to wireframes)
<span style="color:lightgreen; font-size:19pt"> Please Click on the link below to goto Wireframes. </span><br>

#### Completed links to wireframes including Desktop and mobile version [draw.io].
[draw.io]: https://www.draw.io/#G0B2I8nkmioR-4QUlGcGNxX1A3UjA

> Task Completed by Jason Wallace and Gurjinder Ghuman






[//]: <> (tools)
<span style="color:coral; font-size:19pt"> Tools </span>
## Slack
Will be used as our main form of communication when we aren’t in class. We have decided to use Slack as our primary form of communication as we all use slack and all have the mobile apps on our phone thus allowing us to get direct messages instantly.

## Facebook & Gmail
Will be backup forms of communication should Slack not work. We decided to use this as a backup because not all of us are on Facebook all the time or check our emails regularly so it would not be suitable.

## Google Docs
Will be used to write and share reports with group members. Google docs is the best File sharing platform we found because it allows us to edit documents and information with different people interacting and also allows us to comment on each others work

## Trello
Will be used to share ideas for the program

## Draw-io
Will be used to create the wireframes for the program. We decided to use Draw-io to make our wireframes because it is really easy to use and also provides accurate representation of our website we will develop

## Bitbucket
Will be used to host our program and reports. We decided to use Bitbucket Rather than GitHub because it is easer for us to use as we are all used to the platform and are rather confident with it

>Task completed by Michael Tindall and Liam Mason-Webb







